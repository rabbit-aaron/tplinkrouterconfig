from time import sleep
from TpLinkRouterConfig import TpLinkRouterConfig


ROUTER_ADDRESS = ''
USERNAME = ''
PASSWORD = ''
INTERVAL = 5
REFRESH_EVERY = 20
trc = TpLinkRouterConfig(ROUTER_ADDRESS,username=USERNAME,password=PASSWORD)
count = 0
while 1:
  count += 1
  count = count % REFRESH_EVERY
  if count == 0:
    trc.refresh_mac_name_dict()
  print '===================================================================================='
  print '| %-15s | %-15s | %-24s | %-17s |' % ('   IP ADDRESS', '     SPEED','         ALIAS','  MAC ADDRESS')
  for i in trc.net_flow(INTERVAL):
    print '|-----------------|-----------------|--------------------------|-------------------|'
    print i
  print '====================================================================================\n'
  sleep(INTERVAL)
