from base64 import b64encode
from collections import OrderedDict
import httplib2
import re


class TpLinkRouterConfig(object):
  
  __QOS_ADDRESS = '/userRpm/QoSCfgRpm.htm'
  __REGEX_SCRIPT = re.compile(ur'<SCRIPT.*?>([\s\S]*?)</SCRIPT>')
  __REGEX_STATLIST_LINE = re.compile(ur'((?:[0-9A-Za-z\.\-]+\, ){12}(?:\d+))')
  __REGEX_DHCPDYNLIST_LINE = re.compile(ur'(^(?:".*?"\, ){2})')

  @staticmethod
  def __get_QoS_arg(QoS_ctrl, user_wan_type, up_bandwidth, down_bandwidth):
    if user_wan_type == 'ADSL':
      user_wan_type = 0
    else:
      user_wan_type = 1
    return '?QoSCtrl=%d&userWanType=%d&up_bandWidth=%d&down_bandWidth=%d&Save=%%B1%%A3+%%B4%%E6' % (QoS_ctrl, user_wan_type, up_bandwidth, down_bandwidth)

  def __init__(self,router_address,username='admin',password='admin'):
    self.__router_address = router_address
    self.__auth_token = b64encode('%s:%s' % (username,password)).replace('=','')
    self.__headers = {
      'Cookie':'Authorization=Basic%%20%s%%3D' % (self.__auth_token),
    }
    self.__http = httplib2.Http()
    self.refresh_mac_name_dict()

  def __getRequest(self,url):
    (header,content) = self.__http.request(url,'GET',headers=self.__headers)
    if header['status'] != '200':
      raise Exception('Connection failed, HTTP response code: %s' % (header['status']))
    return (header,content)

  def QoS_status(self):
    (header, content) = self.__getRequest('http://%s%s' % (self.__router_address,TpLinkRouterConfig.__QOS_ADDRESS))
    return 'on' if content.split('\n')[2] == '1,' else 'off'
  

  def QoS_on(self, up_bandwidth, down_bandwidth, user_wan_type='ADSL'):
    args = TpLinkRouterConfig.__get_QoS_arg(1,user_wan_type,up_bandwidth,down_bandwidth)
    (header, content) = self.__getRequest('http://%s%s%s' % (self.__router_address,TpLinkRouterConfig.__QOS_ADDRESS,args))
    return content.split('\n')[2] == '1,'

  def QoS_off(self, up_bandwidth, down_bandwidth, user_wan_type='ADSL'):
    args = TpLinkRouterConfig.__get_QoS_arg(0,user_wan_type,up_bandwidth,down_bandwidth)
    (header, content) = self.__getRequest('http://%s%s%s' % (self.__router_address,TpLinkRouterConfig.__QOS_ADDRESS,args))
    return content.split('\n')[2] == '0,'

  def net_flow(self,interval=5,separator='|'):
    result = self.net_flow_dict(interval)
    return [
      '%s %-15s %s %15s %s %-24s %s %-17s %s' % 
        (
          separator,
          k,
          separator,
          '%.2f KB/S' % (v['current_bytes'] / 1024.0),
          separator,
          self.__mac_name_dict.get(v['MAC']) or '',
          separator,
          v['MAC'],
          separator
        ) for (k,v) in result.items()
    ]

  def net_flow_dict(self,interval):
    url = 'http://%s/userRpm/SystemStatisticRpm.htm?interval=%d&sortType=5&Num_per_page=100&Goto_page=1' % (self.__router_address,interval)
    (header, content) = self.__getRequest(url)
    p = TpLinkRouterConfig.__REGEX_SCRIPT
    data = p.match(content).group(0).replace('"','')
    p = TpLinkRouterConfig.__REGEX_STATLIST_LINE
    result = OrderedDict()
    for i in data.split('\n'):
      match = p.match(i)
      if match:
        arr = match.group(0).split(',')
        for i in xrange(3,13):
          arr[i] = int(arr[i])
        result[arr[1].strip()] = {
          'MAC':arr[2].strip(),
          'total_packets':arr[3],
          'total_bytes':arr[4],
          'current_packets':arr[5],
          'current_bytes':arr[6],
          'ICMP_tx': [arr[7],arr[8]],
          'UDP_tx': [arr[9],arr[10]],
          'SYN_tx': [arr[11],arr[12]],
        }
    return result

  def refresh_mac_name_dict(self):
    self.__get_mac_name_dict()
  
  def __get_mac_name_dict(self,update=True):
    result = {}
    url = 'http://%s/userRpm/AssignedIpAddrListRpm.htm' % (self.__router_address)
    (header,content) = self.__getRequest(url)
    p = TpLinkRouterConfig.__REGEX_SCRIPT
    data = p.match(content).group(0)
    p = TpLinkRouterConfig.__REGEX_DHCPDYNLIST_LINE
    for i in data.split('\n'):
      match = p.match(i)
      if match:
        arr = match.group(0).replace('"','').split(', ')
        result[arr[1].strip()] = arr[0].strip()
    if update:
      self.__mac_name_dict = result
    return result
